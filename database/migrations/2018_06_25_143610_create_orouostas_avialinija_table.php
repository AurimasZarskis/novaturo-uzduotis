<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrouostasAvialinijaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orouostas_avialinija', function (Blueprint $table) {
            $table->integer('orouostas_id')->unsigned();
            $table->foreign('orouostas_id')->references('id')
                ->on('oro_uostai')->onDelete('cascade');
            $table->integer('avialinija_id')->unsigned();
            $table->foreign('avialinija_id')->references('id')
                ->on('avialinijos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orouostas_avialinija');
    }
}
