<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');

});
/***
 * Oro Uostu
 */
Route::get('/oro_uostai', 'OroUostaiController@index');
Route::get('/oro_uostai/prideti', 'OroUostaiController@new');
Route::post('/oro_uostai/prideti', 'OroUostaiController@add');
Route::get('/oro_uostai/redaguoti/{id}', 'OroUostaiController@edit');
Route::put('/oro_uostai/redaguoti/{id}', 'OroUostaiController@update');
Route::delete('/oro_uostai/trinti/{id}', 'OroUostaiController@delete');

/***
 * Avialiniju
 */
Route::get('/avialinijos', 'AvialinijosController@index');
Route::get('/avialinijos/prideti', 'AvialinijosController@new');
Route::post('/avialinijos/prideti', 'AvialinijosController@add');
Route::get('/avialinijos/redaguoti/{id}', 'AvialinijosController@edit');
Route::put('/avialinijos/redaguoti/{id}', 'AvialinijosController@update');
Route::delete('/avialinijos/trinti/{id}', 'AvialinijosController@delete');
