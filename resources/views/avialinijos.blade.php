@extends('layouts.main')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Avialinijos</h1>
    </div>
    @include('layouts.messages')
    <a class="btn btn-primary" href="/avialinijos/prideti" role="button">Nauja</a>

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Pavadinimas</th>
                <th scope="col">Šalis</th>
                <th scope="col">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Avialinijos as $Avialinija)

                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$Avialinija->pavadinimas}}</td>
                    <td>{{$Avialinija->salys->pavadinimas}}</td>
                    <td>
                        <a class="btn btn-secondary float-left" href="/avialinijos/redaguoti/{{ $Avialinija->id }}" role="button">Redaguoti</a>
                        <form action="/avialinijos/trinti/{{ $Avialinija->id }}" method="post" class="form-inline float-left">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <button type="submit" class="btn btn-danger">Trinti</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
        {{$Avialinijos->links('layouts.pagination')}}
@endsection