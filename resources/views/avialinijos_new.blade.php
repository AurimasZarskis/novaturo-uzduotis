@extends('layouts.main')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Nauja avialinija</h1>
    </div>
    @include('layouts.messages')
    <form action="/avialinijos/prideti" method="post">
        <div class="form-group">
            <label for="pavadinimas">Apialinijos pavadinimas</label>
            <input type="text" class="form-control" name="pavadinimas" id="pavadinimas" placeholder="">
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Example select</label>
            <select class="form-control" name="salis" id="exampleFormControlSelect1">
                @foreach ($Salys as $Salis)
                    <option value="{{$Salis['salies_kodas']}}">{{$Salis['pavadinimas']}}</option>
                @endforeach
            </select>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary mb-3">Pridėti</button>
    </form>
@endsection