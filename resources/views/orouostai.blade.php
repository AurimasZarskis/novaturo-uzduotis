@extends('layouts.main')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Oro uostai</h1>
    </div>
    @include('layouts.messages')
    <a class="btn btn-primary" href="/oro_uostai/prideti" role="button">Naujas</a>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Pavadinimas</th>
                <th scope="col">Šalis</th>
                <th scope="col">Lokacija(ilguma/platuma)</th>
                <th scope="col">Avialinijos</th>
                <th scope="col">Veiksmai</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Orouostai as $Orouostas)

                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$Orouostas->pavadinimas}}</td>
                    <td>{{$Orouostas->salys->pavadinimas}}</td>
                    <td>{{$Orouostas->longitude}}/{{$Orouostas->latitude}}</td>
                    <td>
                        @foreach($Orouostas->avialinijos as $Avialinija)
                            <span class="badge badge-secondary">{{$Avialinija->pavadinimas}}</span>
                        @endforeach
                    </td>
                    <td>
                        <a class="btn btn-secondary float-left" href="/oro_uostai/redaguoti/{{ $Orouostas->id }}" role="button">Redaguoti</a>
                        <form action="/oro_uostai/trinti/{{ $Orouostas->id }}" method="post" class="form-inline float-left">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <button type="submit" class="btn btn-danger">Trinti</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{$Orouostai->links('layouts.pagination')}}
@endsection