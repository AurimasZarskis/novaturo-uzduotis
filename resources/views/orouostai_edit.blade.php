@extends('layouts.main')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Redaguoti oro uostą</h1>
    </div>
    @include('layouts.messages')
    <form action="/oro_uostai/redaguoti/{{$Orouostas->id}}" method="post">
        <div class="form-group">
            <label for="pavadinimas">Oro uosto pavadinimas</label>
            <input type="text" class="form-control" name="pavadinimas" id="pavadinimas" value="{{$Orouostas->pavadinimas}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Šalis</label>
            <select class="form-control" name="salis" id="exampleFormControlSelect1">
                @foreach ($Salys as $Salis)
                    <option value="{{$Salis->salies_kodas}}" @if($Salis->salies_kodas == $Orouostas->salis) {{'selected'}} @endif>{{$Salis->pavadinimas}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect2">Avialinijos</label>
            <select multiple class="form-control" name="avialinijos[]" id="exampleFormControlSelect2">
                @foreach ($Avialinijos as $Avialinija)
                    <option value="{{$Avialinija->id}}" @foreach($Orouostas->avialinijos as $Orouostas_avialinija)@if($Avialinija->id == $Orouostas_avialinija->id) {{'selected'}} @endif @endforeach >{{$Avialinija->pavadinimas}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" style="width: 400px" id="search" placeholder="Ieškoti">
            <div id="map" style="width: 100%; height: 450px; float: left;"></div>
        </div>
        <div class="form-group">
            <label for="lng">Ilguma</label>
            <input type="text" class="form-control" name="lng" id="lng" value="{{$Orouostas->longitude}}">
            <label for="lat">Platuma</label>
            <input type="text" class="form-control" name="lat" id="lat" value="{{$Orouostas->latitude}}">
        </div>
        {{ csrf_field() }}
        {{ method_field('put') }}
        <button type="submit" class="btn btn-primary mb-3">Atnajinti</button>
    </form>

    <script>
        function initMap() {
            var latitude = parseFloat(document.getElementById('lat').value);
            var longitude = parseFloat(document.getElementById('lng').value);
            console.log(lat);
            var position = {lat: latitude, lng: longitude};

            var map = new google.maps.Map(
                document.getElementById('map'),
                {
                    zoom: 4,
                    center: position
                });
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker(
                {
                    position: position,
                    map: map,
                    draggable: true
                });

            var searchbox = new google.maps.places.SearchBox(document.getElementById('search'));
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            document.getElementById('lat').value=lat;
            document.getElementById('lng').value=lng;

            google.maps.event.addListener(searchbox, 'places_changed', function(){
                var places = searchbox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, pace;

                for(i = 0; place = places[i]; i++){
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                }

                map.fitBounds(bounds);
                map.setZoom(15);
            });

            google.maps.event.addListener(marker, 'position_changed', function(){
                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();

                document.getElementById('lat').value=lat;
                document.getElementById('lng').value=lng;
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBOgwUEsYdyG0-9Z11Ln-D6qfuXxC4uhO8&callback=initMap">
    </script>
@endsection