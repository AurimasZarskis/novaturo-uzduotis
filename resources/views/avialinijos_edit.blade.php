@extends('layouts.main')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Redaguoti avialiniją</h1>
    </div>
    @include('layouts.messages')
    <form action="/avialinijos/redaguoti/{{$Avialinija->id}}" method="post">
        <div class="form-group">
            <label for="pavadinimas">Apialinijos pavadinimas</label>
            <input type="text" class="form-control" name="pavadinimas" id="pavadinimas" value="{{$Avialinija->pavadinimas}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Example select</label>
            <select class="form-control" name="salis" id="exampleFormControlSelect1">
                @foreach ($Salys as $Salis)
                    <option value="{{$Salis->salies_kodas}}" @if($Salis->salies_kodas == $Avialinija->salis) {{'selected'}} @endif>{{$Salis->pavadinimas}}</option>
                @endforeach
            </select>
        </div>
        {{ csrf_field() }}
        {{ method_field('put') }}
        <button type="submit" class="btn btn-primary mb-3">Atnaujinti</button>
    </form>
@endsection