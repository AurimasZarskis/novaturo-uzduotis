<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{ explode('/', Request::path())[0] == '' ? 'active' : '' }}" href="/">
                    <span data-feather="home"></span>
                    Pagrindinis <span class="sr-only"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ explode('/', Request::path())[0] == 'oro_uostai' ? 'active' : '' }}" href="/oro_uostai">
                    Oro uostai
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link {{ explode('/', Request::path())[0] == 'avialinijos' ? 'active' : '' }}" href="/avialinijos">
                    Avialinijos
                </a>
            </li>
        </ul>
    </div>
</nav>