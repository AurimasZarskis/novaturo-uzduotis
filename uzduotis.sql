-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2018 at 10:42 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uzduotis`
--

-- --------------------------------------------------------

--
-- Table structure for table `avialinijos`
--

CREATE TABLE `avialinijos` (
  `id` int(10) UNSIGNED NOT NULL,
  `pavadinimas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `avialinijos`
--

INSERT INTO `avialinijos` (`id`, `pavadinimas`, `salis`, `created_at`, `updated_at`) VALUES
(2, 'British Airways', 'GB / GBR', '2018-06-26 11:22:06', '2018-06-26 11:22:06'),
(3, 'DHL Air UK', 'GB / GBR', '2018-06-26 11:22:23', '2018-06-26 11:22:23'),
(4, 'AirTanker', 'GB / GBR', '2018-06-26 11:22:41', '2018-06-26 11:22:41'),
(5, 'flyLAL', 'LT / LTU', '2018-06-26 11:23:15', '2018-06-26 11:23:15'),
(6, 'Luxair', 'LU / LUX', '2018-06-26 11:23:52', '2018-06-26 11:28:48'),
(7, 'Air Malta', 'MT / MLT', '2018-06-26 11:24:26', '2018-06-26 11:24:26'),
(8, 'LOT', 'PL / POL', '2018-06-26 11:24:49', '2018-06-26 11:24:49'),
(9, 'Ural Airlines', 'RU / RUS', '2018-06-26 11:25:21', '2018-06-26 11:25:21'),
(10, 'Utair', 'RU / RUS', '2018-06-26 11:25:34', '2018-06-26 11:25:34'),
(11, 'Nordstar', 'RU / RUS', '2018-06-26 11:25:50', '2018-06-26 11:25:50'),
(12, 'Turkish Airlines', 'TR / TUR', '2018-06-26 11:26:20', '2018-06-26 11:26:20'),
(13, 'Alaska Airlines', 'US / USA', '2018-06-26 11:26:42', '2018-06-26 11:26:42'),
(14, 'United Airlines', 'US / USA', '2018-06-26 11:26:52', '2018-06-26 11:26:52'),
(15, 'JetBlue Airways', 'US / USA', '2018-06-26 11:27:03', '2018-06-26 11:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_25_134318_create_salis_table', 1),
(4, '2018_06_25_134739_create_oro_oustas_table', 1),
(5, '2018_06_25_134942_create_avialinijas_table', 1),
(6, '2018_06_25_143610_create_orouostas_avialinija_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orouostas_avialinija`
--

CREATE TABLE `orouostas_avialinija` (
  `orouostas_id` int(10) UNSIGNED NOT NULL,
  `avialinija_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orouostas_avialinija`
--

INSERT INTO `orouostas_avialinija` (`orouostas_id`, `avialinija_id`) VALUES
(2, 2),
(2, 3),
(2, 5),
(2, 12),
(2, 14),
(2, 15),
(3, 2),
(3, 3),
(3, 14),
(3, 15),
(4, 5),
(4, 6),
(4, 8),
(4, 9),
(4, 12),
(5, 2),
(5, 4),
(5, 9),
(5, 10),
(5, 11),
(6, 3),
(6, 4),
(6, 5),
(6, 12),
(7, 3),
(7, 4),
(7, 5),
(7, 10),
(7, 11);

-- --------------------------------------------------------

--
-- Table structure for table `oro_uostai`
--

CREATE TABLE `oro_uostai` (
  `id` int(10) UNSIGNED NOT NULL,
  `pavadinimas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` decimal(10,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oro_uostai`
--

INSERT INTO `oro_uostai` (`id`, `pavadinimas`, `salis`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(2, 'London Heathrow Airport', 'GB / GBR', '51.47002230', '-0.45429550', '2018-06-26 11:32:42', '2018-06-26 11:32:42'),
(3, 'LAX', 'US / USA', '33.94158890', '-118.40853000', '2018-06-26 11:34:22', '2018-06-26 11:34:22'),
(4, 'Warsaw', 'PL / POL', '52.16723690', '20.96789110', '2018-06-26 11:38:21', '2018-06-26 11:38:21'),
(5, 'Bykovo Airport', 'RU / RUS', '55.62214849', '38.06351160', '2018-06-26 11:42:50', '2018-06-26 11:43:10'),
(6, 'Tarptautinis Vilniaus oro uostas', 'LT / LTU', '54.63803660', '25.28655840', '2018-06-26 11:43:54', '2018-06-26 11:43:54'),
(7, 'Kauno oro uostas', 'LT / LTU', '54.96728000', '24.07238000', '2018-06-26 11:44:38', '2018-06-26 12:30:13');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salys`
--

CREATE TABLE `salys` (
  `salies_kodas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pavadinimas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salys`
--

INSERT INTO `salys` (`salies_kodas`, `pavadinimas`) VALUES
('GB / GBR', 'United Kingdom'),
('LT / LTU', 'Lithuania'),
('LU / LUX', 'Luxembourg'),
('MT / MLT', 'Malta'),
('PL / POL', 'Poland'),
('RU / RUS', 'Russia'),
('TR / TUR', 'Turkey'),
('US / USA', 'United States');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avialinijos`
--
ALTER TABLE `avialinijos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `avialinijos_salis_foreign` (`salis`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orouostas_avialinija`
--
ALTER TABLE `orouostas_avialinija`
  ADD KEY `orouostas_avialinija_orouostas_id_foreign` (`orouostas_id`),
  ADD KEY `orouostas_avialinija_avialinija_id_foreign` (`avialinija_id`);

--
-- Indexes for table `oro_uostai`
--
ALTER TABLE `oro_uostai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oro_uostai_salis_foreign` (`salis`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `salys`
--
ALTER TABLE `salys`
  ADD PRIMARY KEY (`salies_kodas`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `avialinijos`
--
ALTER TABLE `avialinijos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oro_uostai`
--
ALTER TABLE `oro_uostai`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `avialinijos`
--
ALTER TABLE `avialinijos`
  ADD CONSTRAINT `avialinijos_salis_foreign` FOREIGN KEY (`salis`) REFERENCES `salys` (`salies_kodas`) ON DELETE CASCADE;

--
-- Constraints for table `orouostas_avialinija`
--
ALTER TABLE `orouostas_avialinija`
  ADD CONSTRAINT `orouostas_avialinija_avialinija_id_foreign` FOREIGN KEY (`avialinija_id`) REFERENCES `avialinijos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orouostas_avialinija_orouostas_id_foreign` FOREIGN KEY (`orouostas_id`) REFERENCES `oro_uostai` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oro_uostai`
--
ALTER TABLE `oro_uostai`
  ADD CONSTRAINT `oro_uostai_salis_foreign` FOREIGN KEY (`salis`) REFERENCES `salys` (`salies_kodas`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
