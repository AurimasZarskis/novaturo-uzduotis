<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OroOustas;
use App\Salis;
use App\Avialinija;
use Mapper;

class OroUostaiController extends Controller
{
    public function index(){
        $orouostai = OroOustas::with('salys', 'avialinijos')->paginate(10);
        return view('orouostai')->with('Orouostai', $orouostai);
    }

    public function new(){
        $salys = Salis::all();
        $avialinijos = Avialinija::all();
        return view('orouostai_new', ['Salys' => $salys, 'Avialinijos' => $avialinijos]);
    }

    public function add(Request $request){
        $request->validate([
            'pavadinimas' => 'required',
            'salis' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ],
        [
            'pavadinimas.required' => 'Neįvestas oro uosto pavadinimas',
            'salis.required' => 'Nepasirinkta jokia šalis',
            'lat.required' => 'Neįvesta platuma',
            'lng.required' => 'Neįvesta ilguma',
        ]);

        $orouostas = new OroOustas;
        $orouostas->pavadinimas = $request->input('pavadinimas');
        $orouostas->salis = $request->input('salis');
        $orouostas->latitude = $request->input('lat');
        $orouostas->longitude = $request->input('lng');
        $orouostas->save();
        $orouostas->avialinijos()->attach($request->input('avialinijos'));

        return redirect('/oro_uostai')->with('Success', "Pridėts naujas oro uostas");
    }

    public function Edit($id){
        $Salys = Salis::all();
        $avialinijos = Avialinija::all();
        $Orouostas = OroOustas::where('id', $id)->first();
        return view('orouostai_edit', ['Salys' => $Salys, 'Orouostas' => $Orouostas, 'Avialinijos' => $avialinijos]);
    }

    public function Update(Request $request, $id){
        $request->validate([
            'pavadinimas' => 'required',
            'salis' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ],
        [
            'pavadinimas.required' => 'Neįvestas oro uosto pavadinimas',
            'salis.required' => 'Nepasirinkta jokia šalis',
            'lat.required' => 'Neįvesta platuma',
            'lng.required' => 'Neįvesta ilguma',
        ]);

        $orouostas = OroOustas::where('id', $id)->first();
        $orouostas->pavadinimas = $request->input('pavadinimas');
        $orouostas->salis = $request->input('salis');
        $orouostas->latitude = $request->input('lat');
        $orouostas->longitude = $request->input('lng');
        $orouostas->save();
        //$orouostas->avialinijos()->delete();
        $orouostas->avialinijos()->sync($request->input('avialinijos'));
        return redirect('/oro_uostai')->with('Success', 'Oro uostas atnaujinta');
    }

    public function Delete($id){
        OroOustas::where('id', $id)->delete();
        return redirect('/oro_uostai')->with('Success', 'Oro uostas pašalinta');
    }
}
