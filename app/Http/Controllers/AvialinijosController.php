<?php

namespace App\Http\Controllers;

use App\Salis;
use Illuminate\Http\Request;
use App\Avialinija;

class AvialinijosController extends Controller
{
    public function New(Request $request){
        $Salys = Salis::all();
        return view('avialinijos_new')->with('Salys', $Salys);
    }

    /***
     *
     * Prideti nauja avialinija
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function Add(Request $request){
        $request->validate([
            'pavadinimas' => 'required',
            'salis' => 'required',
        ],
        [
            'pavadinimas.required' => 'Neįvestas avialinijos pavadinimas',
            'salis.required' => 'Nepasirinkta jokia šalis',
        ]);

        $avialinija = new Avialinija;
        $avialinija->pavadinimas = $request->input('pavadinimas');
        $avialinija->salis = $request->input('salis');
        $avialinija->save();

        return redirect('/avialinijos')->with('Success', "Pridėta nauja avialinija");
    }

    public function Index(){
        $avialinijos = Avialinija::with('salys')->paginate(10);

        return view('avialinijos')->with('Avialinijos', $avialinijos);
    }

    public function Edit($id){
        $Salys = Salis::all();
        $Avialinija = Avialinija::where('id', $id)->first();
        return view('avialinijos_edit', ['Salys' => $Salys, 'Avialinija' => $Avialinija]);
    }

    public function Update(Request $request, $id){
        $request->validate([
            'pavadinimas' => 'required',
            'salis' => 'required',
        ],
        [
            'pavadinimas.required' => 'Neįvestas avialinijos pavadinimas',
            'salis.required' => 'Nepasirinkta jokia šalis',
        ]);

        $avialinija = Avialinija::where('id', $id)->first();
        $avialinija->pavadinimas = $request->input('pavadinimas');
        $avialinija->salis = $request->input('salis');
        $avialinija->save();
        return redirect('/avialinijos')->with('Success', 'Avialinija atnaujinta');
    }

    public function Delete($id){
        Avialinija::where('id', $id)->delete();
        return redirect('/avialinijos')->with('Success', 'Avialinija pašalinta');
    }
}
