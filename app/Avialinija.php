<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avialinija extends Model
{
    protected $table = 'avialinijos';
    public function salys()
    {
        return $this->belongsTo('App\Salis', 'salis', 'salies_kodas');
    }

    public function oroUostai(){
        return $this->belongsToMany('App\OroUostas', 'orouostas_avialinija', 'avialinija_id', 'orouostas_id');
    }
}
