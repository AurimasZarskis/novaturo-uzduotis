<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OroOustas extends Model
{
    protected $table = 'oro_uostai';
    public function avialinijos(){
        return $this->belongsToMany('App\Avialinija', 'orouostas_avialinija', 'orouostas_id', 'avialinija_id');
    }

    public function salys()
    {
        return $this->belongsTo('App\Salis', 'salis', 'salies_kodas');
    }
}
