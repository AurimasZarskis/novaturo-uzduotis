<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salis extends Model
{
    protected $table = 'salys';
    public function avialinijos()
    {
        return $this->hasMany('App\Avialinija', 'salis', 'salies_kodas');
    }
}
